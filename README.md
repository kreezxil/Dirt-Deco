[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## Description

Build your homes and other buildings out of dirt and similar materials.

![](https://i.imgur.com/MdIPA6o.png)

## Slabs

*   ![](https://i.imgur.com/HpELsAB.png)
    Dirt
*   ![](https://i.imgur.com/qEKwFzF.png)
    Grass
*   ![](https://i.imgur.com/95LNb3y.png)
    Sand
*   ![](https://i.imgur.com/2z5UUYm.png)
    Red Sand
*   ![](https://i.imgur.com/jeeVSen.png)
    Gravel

## Stairs

*   ![](https://i.imgur.com/4FwkN4W.png)
    Dirt
*   ![](https://i.imgur.com/OAmE02P.png)
    Grass
*   ![](https://i.imgur.com/sN466o0.png)
    Sand
*   ![](https://i.imgur.com/EC2V9ux.png)
    Red Sand
*   ![](https://i.imgur.com/bOy1ZrC.png)
    Gravel

## Blocks

*   ![](https://i.imgur.com/pzuwS2a.png)
    A recipe actually, to get grass easily, dirt surrounded by wheat seeds!

## Todo

*   Doors
*   Fences
*   Fence Gates
*   Trap Doors

## Modpacks

Don't ask just add, linking back here will be great.

## MCBBS

MCBBS (China), yes you may repost this there but please make all downloads link back here.

## LICENSE

MIT

## Help a Veteran today

I am Veteran of United States Army. I am not legally disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [ ![](https://i.imgur.com/WhT4CbN.png) ](https://patreon.com/kreezxil).
